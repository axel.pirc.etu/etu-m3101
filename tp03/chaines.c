#include <stdio.h>
#include "chaines.h"

int mon_strlen_tab(char s[]){
    int i = 0;
    while(s[i] != '\0') i++;
    return i;
}

int mon_strlen(const char *s){
    int i=0;
    while(*(s+i) != '\0') i++;
    return i;
}

int mon_strcmp(const char * s1, const char * s2){
    int size = mon_strlen(s1);
    if(mon_strlen(s1) < mon_strlen(s2)) size = mon_strlen(s2);
    return mon_strncmp(s1, s2, size);
}

int mon_strncmp(const char * s1, const char * s2, int n){
    int cpt=0;
    while(cpt < n){
        if(s1[cpt] != s2[cpt]) return s1[cpt]-s2[cpt];
        cpt++;
    }
    return 0;
}

char *mon_strcat(char *dest, const char *src){
    int size = mon_strlen(dest);
    int cpt=0;

    for(; *src != '\0'; src++){
        *(dest + size + cpt) = *src;
        cpt++;
    }

    *(dest+cpt+size) = '\0';
    return dest;
}

char *mon_strchr(const char *s, int c){
    for(; *s != '\0'; s++) if(*s==c) return (char *)s;
    return NULL;
}

/*char *mon_strstr(const char *haystack, const char *needle){
    if(*needle == '\0') return (char *)haystack;
    int cpt=0;

    for(; *(haystack+cpt) != '\0'; cpt++){
        if(*(haystack+cpt) == *(needle)){
            int result=1;
            int cpt2 = 1;
            while(result == 1 && *(needle+cpt2) != '\0'){
                if(*(haystack+cpt+cpt2) != *(needle+cpt2)) result=0;
                else cpt2++;
            }
            if(result==1) return (char *)(haystack+cpt);
        }
    }
    return NULL;
}*/
char *mon_strstr(const char *haystack, const char *needle){
    if(*needle == '\0') return (char *)haystack;
    int needleSize = mon_strlen(needle);

    for(; *haystack != '\0'; haystack++){
        if(mon_strncmp(haystack, needle, needleSize) == 0) return (char *)(haystack);
    }
    return NULL;
}

int mon_strncmpIgnoreCase(const char * s1, const char * s2, int n){
    int cpt=0;
    while(cpt < n){
        char tmp1 = s1[cpt];
        char tmp2 = s2[cpt];

        if(tmp1 >= 98 && tmp1 <= 122) tmp1-=32;
        if(tmp2 >= 98 && tmp2 <= 122) tmp2-=32;

        if(s1[cpt] != s2[cpt]) return s1[cpt]-s2[cpt];
        cpt++;
    }
    return 0;
}

// pareil que strstr mais sans prendre en compte maj/min
char *mon_strcasestr(const char *haystack, const char *needle){
    if(*needle == '\0') return (char *)haystack;
    int needleSize = mon_strlen(needle);

    for(; *haystack != '\0'; haystack++){
        if(mon_strncmpIgnoreCase(haystack, needle, needleSize) == 0) return (char *)(haystack);
    }
    return NULL;
}

//pareil que strchr mais dernière occurence
char *mon_strrchr(const char *s, int c){
    int size = mon_strlen(s)-1;
    for(; size>=0; size--) if(*(s+size) == c) return (char *)(s+size);
    return NULL;
}