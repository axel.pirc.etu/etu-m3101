#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fonctions.h"
#include "tests.h"
#include <ctype.h>
#define TAILLE 80

char * miroir(const char *s){
    int length=strlen(s);
    char* strMiroir = malloc(length+1);

    if(!strMiroir) return NULL;

    length--;
    int cptMiroir = 0;
    while(length >= 0){
        strMiroir[cptMiroir] = s[length];
        cptMiroir++; length--;
    }
    strMiroir[cptMiroir]='\0';
    return strMiroir;
}

char * saisie(){
    char *c = malloc(TAILLE);
    int compteurTaille = 1;
    int cpt=0;
    char act = getchar();
    while(!isspace(act) && act != EOF){
        if(cpt==TAILLE*compteurTaille){
            compteurTaille++;
            c = realloc(c, TAILLE*compteurTaille);
        }
        c[cpt]=act;
        cpt++;
        act=getchar();
    }
    c = realloc(c, cpt+1);
    c[cpt] = '\0';
    return c;
}
