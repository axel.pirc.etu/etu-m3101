#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fonctions.h"

int erreur(){
    printf("mauvaise utilisation\n");
    return 1;
}

int main(int argc, char **argv){
    if(argc == 1) return erreur();

    int b_saisie = 0;
    int b_miroir = 0;
    int nbMots = 0;
    char *mot = NULL;
    char *lettreMot = NULL;
    for (int i = 1 ; i < argc ; ++i) {
        if (argv[i][0] == '-'){
            if(!b_saisie && strstr(argv[i], "s") != NULL) b_saisie=1;
            if(!b_miroir && strstr(argv[i], "m") != NULL) b_miroir=1;
        }else{
            nbMots++;
            mot = argv[i];
        }
    }

    if(nbMots > 1) return erreur(); //si plus d'un mot en paramètre
    if(b_saisie){ //si saisie
        if(nbMots == 0){
            mot = saisie(); //si aucune mot, on fait la saisie
            lettreMot = mot;
        }
        else return erreur(); //sinon, on return erreur
    }
    if(!b_saisie && nbMots == 0) return erreur(); //si pas de saisie et pas de mot

    if(b_miroir) mot = miroir(mot);
    printf("%s\n", mot);
    free(mot);
    if(b_saisie) free(lettreMot);
    return 0;
}

