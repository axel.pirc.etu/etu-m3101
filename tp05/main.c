#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "wordcount.h"
#include <stdlib.h>
#define TAILLE 80

void resultats(char *file, int args, int b_car, int b_mot, int b_lig, int car, int mot, int lig){
    if(args){
        printf("Caractères: %5i Mots: %5i Lignes: %5i ", car, mot, lig);
    }else{
        if(b_car) printf("Caractères: %5i ", car);
        if(b_mot) printf("Mots: %5i ", mot);
        if(b_lig) printf("Lignes: %5i ", lig);
    }
    printf("--> %s\n", file);
}

void afficher(char *file, int args, int b_car, int b_mot, int b_lig, int *total_car, int *total_lig, int *total_mot){
    int mot = 0;
    int car = 0;
    int lig = 0;
    traiterFichier(file,&car,&mot,&lig);
    *total_car+=car;
    *total_lig+=lig;
    *total_mot+=mot;

    resultats(file, args, b_car, b_mot, b_lig, car, mot, lig);
}

void afficherStdin(int args, int b_car, int b_mot, int b_lig){
    int mot = 0;
    int car = 0;
    int lig = 0;
    traiter(0,&car,&mot,&lig);

    printf("\n");
    resultats("stdin", args, b_car, b_mot, b_lig, car, mot, lig);
}

int main(int argc, char **argv){
    int flag_args = 1;
    int flag_car = 0;
    int flag_mot = 0;
    int flag_lig = 0;
    int flag_start = 1;

    int file_number=0;
    int total_mot = 0;
    int total_lig = 0;
    int total_car = 0;

    while(*++argv != NULL){
        if(flag_start && *argv[0] == '-'){
            if(flag_args) flag_args=0;
            if(!flag_car && strstr(*argv, "c")) flag_car=1;
            if(!flag_mot && strstr(*argv, "w")) flag_mot=1;
            if(!flag_lig && strstr(*argv, "l")) flag_lig=1;
        }else{
            file_number++;
            if(flag_start) flag_start=0;
            afficher(*argv, flag_args, flag_car, flag_mot, flag_lig, &total_car, &total_lig, &total_mot);
        }
    }
    if(flag_start) afficherStdin(flag_args, flag_car, flag_mot, flag_lig);
    else if(file_number > 1){
        printf("\n");    
        resultats("total", flag_args, flag_car, flag_mot, flag_lig, total_car, total_mot, total_lig);
    }
    argc++;
    return 0;
}