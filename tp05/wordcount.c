#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <unistd.h>
#define BUFFER_SIZE 80

int traiter (int f, int *car, int *mot, int *lig){
    char* buffer = malloc(BUFFER_SIZE);
    if(buffer == NULL){
        printf("Erreur: malloc null.\n");
        exit(1);
    }
    int reader;
    int estMot = 0;
    do{
        reader = read(f,buffer,BUFFER_SIZE);
        if(reader == -1) exit(1);
        int cpt = 0;
        while(cpt != reader){
            if(buffer[cpt] == '\n'){
                (*lig)++;
                estMot=0;
            }else if(isspace(buffer[cpt])) estMot=0;
            else if(!estMot){
                estMot=1;
                (*mot)++;
            }
            (*car)++;
            cpt++;
        }
    }while(reader);
    free(buffer);
    return 0;
}

int traiterFichier(char *file, int *car, int *mot, int *lig){
    int fd = open(file, O_RDONLY);
    if(fd==-1){
        printf("Erreur fd = -1.\n");
        exit(1);
    }
    int result = traiter(fd,car,mot,lig);
    if(close(fd)==-1){
        printf("Close fd = -1.\n");
        exit(1);
    }
    return result;
}

int _main(void){
    int fd = open("./test",O_RDONLY);
    if(fd==-1){
        printf("Erreur fd = -1.\n");
        exit(1);
    }
    int mot = 0;
    int car = 0;
    int lig = 0;
    traiter(fd,&car,&mot,&lig);
    if(close(fd)==-1){
        printf("Close fd = -1.\n");
        exit(1);
    }
    printf("Caractères: %i, Mots: %i, Lignes: %i\n", car, mot, lig);
    return 0;
}