#include "entiers.h"
#include <stdio.h>

void afficher(int liste[], int taille){
    for(int i=0; i<taille; i++) printf("%d ", liste[i]);
    printf("\n");
}

int somme(int liste[], int taille){
    int result = 0;
    for(int i=0; i<taille; i++) result+=liste[i];
    return result;
}

void copie_dans(int dest[], int src[], int taille){
    for(int i=0; i<taille; i++) dest[i]=src[i];
}

void ajoute_apres(int dest[], int taille_dest, int src[], int taille_src){
    for(int i=0; i<taille_src; i++) dest[i+taille_dest] = src[i];
}