#include "rat.h"

int findPGCD(int nb1, int nb2){
    int result = -1;
    int pgcd = 1;
    if(nb1<0) nb1=0-nb1;
    if(nb2<0) nb2=0-nb2;
    do{
        if(nb1>nb2){
            result=nb1-nb2;
            nb1=result;
        }else{
            result=nb2-nb1;
            nb2=result;
        }
        if(result != 0) pgcd=result;
    }while(result != 0);
    return pgcd;
}

struct rat rat_simplifie(struct rat result){
    int num = result.num;
    int den = result.den;
    int pgcd = findPGCD(num, den);
    struct rat t;
    t.num = num/pgcd;
    t.den = den/pgcd;
    return t;
}

struct rat rat_produit(struct rat n1, struct rat n2){
    struct rat result;
    result.den = n1.den*n2.den;
    result.num = n1.num*n2.num;
    return rat_simplifie(result);
}

struct rat rat_somme(struct rat n1, struct rat n2){
    struct rat result;
    result.den = n1.den*n2.den;
    result.num = n1.num*n2.den+n2.num*n1.den;
    return rat_simplifie(result);
}

struct rat rat_plus_petit(struct rat list[]){
    struct rat minus = list[0];
    if(minus.den == 0){
        minus.num = 1;
        return minus;
    }
    int i=1;
    while(list[i].den != 0){
        if(list[i].num*minus.den < minus.num*list[i].den) minus=list[i];
        i++;
    }
    return minus;
}